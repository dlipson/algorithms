# Algorithms
An overview of abstract data types and their operations, sorting, and other fundamental concepts of algorithm design.
Based on material from CS240 at the University of Waterloo, and *Cracking the Coding Interview (4th Edition)*

## Sorting / Searching
Overview of functionality and runtime:
- [x] Merge Sort
- [x] Heap Sort
- [x] Quick Sort (and Select)
- [x] Binary Search

## Data Structures
Overview of construction, structure, as well as basic operations (search, insert, delete) and their time complexity:
- [x] Linked Lists
- [x] Heaps
    - [x] Min vs Max Heaps
    - [x] Bubble Up/Down
- [x] Binary Search Trees
- [ ] Balanced Search (AVL) Trees
- [x] Graphs
    - [x] DFS/BFS
    - [ ] Shortest Path
- [x] Tries
    - [ ] Suffix Tries and Matching
    - [ ] Huffman Encoding
- [x] Hash Tables (Python Dictionaries)

## Algorithm Design
- [ ] Divide and Conquery
- [ ] Greedy Algorithms
- [ ] Dynamic Programming

## Example Problems

### *Cracking the Coding Interview*
- [x] Chapter 1: Arrays and Strings
- [x] Chapter 2: Linked Lists
- [ ] Chapter 3: Stacks and Queues
- [ ] Chapter 4: Trees and Graphs
- [ ] Chapter 8: Recursion
- [ ] Chapter 9: Sorting and Searching
