import sys

sys.path.append("../../sorting")
from merge_sort import *

a = [3,1,5,7,2,0,-1,1,2,3]
b = [0,1,2,3,4,5,4,3,2,1]
c = [10,9,8,7,6,5,4,3,2,1]

print("A: " + str(a))
print("B: " + str(b))
print("C: " + str(c))

print("SORTING...")

a = merge_sort(a, len(a))
b = merge_sort(b, len(b))
c = merge_sort(c, len(c))

print("A: " + str(a))
print("B: " + str(b))
print("C: " + str(c))
