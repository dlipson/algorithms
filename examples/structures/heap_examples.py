from random import randint
import sys

sys.path.append("../../structures")
from heap import *

h = heap([])

h.insert(5)
h.insert(4)
h.insert(2)
h.insert(7)
h.insert(0)
h.insert(3)

print("Insert Order: 5, 4, 2, 7, 0, 3")

# Expected output: [7, 5, 3, 4, 0, 2]

print(h.a)

print("Deleting Max Element")

h.delete_max()

# Expected output: [5, 4, 3, 2, 0]

print(h.a)
