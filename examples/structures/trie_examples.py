import sys

sys.path.append("../../structures")
from trie import *

t = trie()

print "Inserting 'tell', 'teller', 'dog'"
t.insert("tell")
t.insert("teller")
t.insert("dog")

t.output(t.root)

print "Searching for 'tell': " + str(t.search("tell"))
print "Searching for 'te': " + str(t.search("te"))
print "Searching for 'teller': " + str(t.search("teller"))
print "Searching for 'dog': " + str(t.search("dog"))
print "Searching for '': " + str(t.search(""))

print "Inserting 'teach'"
t.insert("teach")


print "Deleting 'tell' (remove flag)"
t.delete("tell")

t.output(t.root)

print "Searching for 'tell': " + str(t.search("tell"))
print "Searching for 'te': " + str(t.search("te"))
print "Searching for 'teller': " + str(t.search("teller"))
print "Searching for 'teach': " + str(t.search("teach"))

print "Deleting 'teach' (remove until fork)"
t.delete("teach")
t.output(t.root)

print "Deleting 'teller' (remove until root)"
t.delete("teller")
t.output(t.root)

print "Searching for 'tell': " + str(t.search("tell"))
print "Searching for 'te': " + str(t.search("te"))
print "Searching for 'teller': " + str(t.search("teller"))
print "Searching for 'dog': " + str(t.search("dog"))
print "Searching for '': " + str(t.search(""))
print "Searching for 'teach': " + str(t.search("teach"))