import sys
from random import randint

sys.path.append("../../structures")
from binary_tree import *

root = None

for i in xrange(7):
    v = randint(0,10)
    print("INSERT: " + str(v))
    root = insert(root, v)

pre_output(root)

for i in xrange(3):
    v = randint(0,10)
    print("DELETING: " + str(v))
    root = delete(root, v)

pre_output(root)
