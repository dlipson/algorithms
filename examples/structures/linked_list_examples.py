from random import randint
import sys

sys.path.append("../../structures")
from linked_list import *

print("UNSORTED LINKED LIST")

u = unsorted_linked_list()

print "INSERTING 4,7,-1,2,3"
u.insert(4)
u.insert(7)
u.insert(-1)
u.insert(2)
u.insert(3)
u.output()

print "DELETING MAX"
u.delete_max()
u.output()

print "DELETING MAX"
u.delete_max()
u.output()

print "DELETING MAX"
u.delete_max()
u.output()


print("SORTED LINKED LIST")

u = sorted_linked_list()

print "INSERTING 4,7,-1,2,3"
u.insert(4)
u.insert(7)
u.insert(-1)
u.insert(2)
u.insert(3)
u.output()

print "DELETING MAX"
u.delete_max()
u.output()

print "DELETING MAX"
u.delete_max()
u.output()

print "DELETING MAX"
u.delete_max()
u.output()
