# Binary search is a D/C algorithm for searching a sorted array for some k

def binary_search(a, k):
	i = 0
	j = len(a) - 1
	while i < j:
		mid = (i + j) // 2
		if a[mid] == k:
			return True
		elif a[mid] > k:
			j = mid - 1
		else:
			i = mid + 1
	return False

