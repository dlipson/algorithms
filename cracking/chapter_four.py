# Tree/Graph Problems

import sys

sys.path.append("../structures")
from binary_tree import *

#################### 4.1 ####################
def max_depth(root):
	if root == None:
		return 0

	return 1 + max(max_depth(root.left), max_depth(root.right))

def min_depth(root):
	if root == None:
		return 0

	return 1 + min(min_depth(root.left), min_depth(root.right))

def is_balanced(root):
	return abs(max_depth(root) - min_depth(root)) <= 1



#################### 4.2 ####################
# this is the same as DFS/BFS


#################### 4.3 ####################
# keep inserting middle element from either side of array
# to make as stable as possible

