# Arrays and Strings Sample Problems


import sys

sys.path.append("../sorting")
from quick_sort import *


#################### 1.1 ####################
def unique_chars_a(string):
	chars = {}
	for c in string:
		if c in chars:
			return False
		else:
			chars[c] = True
	return True

# no additional structures
# we can sort and then check pairs
def unique_chars_b(string):
	# technically, we are using an additional array 
	# because not reimplementing quick sort, 
	# instead just copy from sorting/quick_sort
	string = quick_sort(list(string))
	for i in xrange(len(string) - 1):
		if string[i] == string[i + 1]:
			return False
	return True


#################### 1.2 ####################
# here we take c style strings (e.g ['d', 'o', 'g', None])
def reverse_string(string):
	i = 0
	j = len(string) - 2
	while i < j:
		temp = string[i]
		string[i] = string[j]
		string[j] = temp
		i += 1
		j -= 1
	return string


#################### 1.4 ####################
def anagrams(a, b):
	chars = {}
	for c in a:
		if c in chars:
			chars[c] += 1
		else:
			chars[c] = 1
	for c in b:
		if c in chars:
			chars[c] -= 1
		else:
			return False
	for c in chars:
		if chars[c] != 0:
			return False
	return True



#################### 1.6 ####################
# try another time


#################### 1.7 ####################
def set_zero(a):
	b = [[None]*len(i) for i in a]
	for y in xrange(len(a)):
		for x in xrange(len(a[y])):
			if b[y][x] != None:
				continue
			elif a[y][x] == 0:
				for x_inner in xrange(len(a[y])):
					b[y][x_inner] = 0
				for y_inner in xrange(len(a)):
					b[y_inner][x] = 0
			else:
				b[y][x] = a[y][x]
	return b



#################### 1.8 ####################
# this is provided in the example
def substring(a, b):
	return a in b

def rotation(a, b):
	concat_a = a + a
	return len(a) == len(b) and substring(b, concat_a)


