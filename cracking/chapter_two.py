# Linked List Sample Problems

import sys

sys.path.append("../structures")
from linked_list import *

#################### 2.2 ####################
def nth_last(ll, n):
	current = ll.root
	nodes = 0

	if n < 1:
		return None

	# note: we could also implement the linked list to store its size
	# this would remove the need for this while loop:
	while current != None:
		current = current.next
		nodes += 1
	
	if nodes < n:
		return None

	current = ll.root
	# we could also potentially implement a storing array so we don't
	# need to iterate through the list twice
	for i in xrange(nodes - n):
		current = current.next
	return current

#################### 2.4 ####################
def sum(a, b):
	new_ll = unsorted_linked_list()
	nodes = []
	carry = 0
	current_a = a.root
	current_b = b.root
	while current_b != None or current_a != None:
		digit = 0
		if current_a != None:
			digit += current_a.value
			current_a = current_a.next
		if current_b != None:
			digit += current_b.value
			current_b = current_b.next
		digit += carry
		carry = digit // 10
		digit = digit % 10
		print(carry, digit)
		nodes.insert(0, digit)
	for n in nodes:
		new_ll.insert(n)
	return new_ll


#################### Additional ####################
# write function to reverse a linked list

def reverse(ll):
	current = ll.root
	previous = None
	while current != None:
		temp_next = current.next
		current.next = previous
		temp = current
		current = temp_next
		previous = temp
	ll.root = previous

# alternatively, can simply insert from start into unsorted_linked_list
# involves a copy linked list
def reverse_copy(ll):
	temp_ll = unsorted_linked_list()
	current = ll.root
	while current != None:
		temp_ll.insert(current.value)
		current = current.next
	ll.root = temp_ll.root
