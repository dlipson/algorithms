# Determine if P is a substring of T
# We can use many different algorithms

# Brute force (for each index of T, start checking for P) is O(|P|*|T|)
# KMP is Ø(|T|)
# Boyer-Moore is O(|T| + |E|), where E is the alphabet

# Rabin-Karp uses hashing, is expected O(|T| + |P|), but unlikely worst case is Ø(|T||P|)
# Does not require extra space only and only takes O(|P|) to construct

# Suffix Tries
# good for many patterns
# if some suffix of T has prefix P, then it is a substring
# O(n2) to build trie, O(|P|) to search