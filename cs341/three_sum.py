# NSUM: Given some array, determine if there is a unique set of N elements that sum to m
# These will return the indexes of the elements in the sorted array
# This is an example of reduction

def two_sum(a, m):
    # sort first if just calling two_sum
    i = 0
    j = len(a) - 1
    solutions = []
    while j > i:
            v = a[i] + a[j]
            if v == m:
                    solutions.append((i, j))
            if v > m:
                    j -= 1
            else:
                    i += 1
    return solutions

def three_sum(a, m):
    # sort array before each two_sum call
    sorted_a = a
    sorted_a.sort()

    solutions = []

    # two sum finds where a[i] + a[j] = m
    # for each v in a, check two sum for a[i] + a[j] = m - v
    for i in xrange(len(sorted_a)):
        result = two_sum(sorted_a, m - sorted_a[i])
        for s in result:
            if i != s[0] and i != s[1]:
                solutions.append((i, s[0], s[1]))
    return solutions

# given three separate arrays x,y,z 
# determine if we can create m where each value is from a different array
def three_arrays_sum(x,y,z,m):
    sorted_a = x + y + z
    sorted_a.sort()

    solutions = []

    for i in xrange(len(sorted_a)):
        result = two_sum(sorted_a, m - sorted_a[i])
        for s in result:
            if i != s[0] and i != s[1]:
                a, b, c = sorted_a[s[0]], sorted_a[s[1]], sorted_a[i]
                # new condition: all from different arrays
                case_one = a in x and b in y and c in z
                case_two = a in x and b in z and c in y
                case_three = a in y and b in x and c in z
                case_four = a in y and b in z and c in x
                case_five = a in z and b in x and c in y
                case_six = a in z and b in y and c in x

                if case_six or case_five or case_four or case_three or case_two or case_one:
                    solutions.append((i, s[0], s[1]))
    return solutions
