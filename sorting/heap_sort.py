# This sorting algorithm uses the heap ADT to sort in O(nlogn) time
import sys

sys.path.append("../structures")
from heap import *

# takes an array, creates a heap and then returns a sorted array from deleting max on whole heap
def heap_sort(a):
    sorted_array = []
    h = heap(a)
    n = len(h.a)
    for i in xrange(n):
        sorted_array.append(h.delete_max()) 
    return list(reversed(sorted_array))

