# Merge Sort is a D/C approach that splits up subarrays and sorts them.
# Has complexity of O(nlogn) since we split into halves, run recursively on each, and apply merge to both halves

import math

# actually join subarrays together
def merge(left, n_left, right, n_right):
    left_index = 0
    right_index = 0
    s = []
    
    # add next biggest element from subarrays
    while left_index < n_left and right_index < n_right:
        if left[left_index] <= right[right_index]:
            s.append(left[left_index])
            left_index += 1
        else:
            s.append(right[right_index])
            right_index += 1

    # one of the subarrays still has elements to add...
    while left_index < n_left:
        s.append(left[left_index])
        left_index += 1
    
    while right_index < n_right:
        s.append(right[right_index])
        right_index += 1
        
    return s

# sort n-sized array a
def merge_sort(a, n):
    if n == 1:
        s = a
    else:
        # split into subarrays
        n_left = int(math.ceil(n/2.))
        n_right = int(math.floor(n/2.))

        a_left = a[:n_left]
        a_right = a[n_left:]
   
        # recursive calls
        s_left = merge_sort(a_left, n_left)
        s_right = merge_sort(a_right, n_right)

        # merge 
        s = merge(s_left, n_left, s_right, n_right)
    return s