# Quick sort is a divide and conquer algorithm for sorting an array
# It uses a pivot so sort elements greater/less than the pivot value
# quick_select for finding the element at position k in sorted array a

from random import randint

# randomized algorithm to choose a pivot
# runtime ***
def choose_pivot(a):
	return randint(0, len(a) - 1)

# partition
# actually sorts based on p partition
def partition(a, p):
	n = len(a)

	temp = a[0]
	a[0] = a[p]
	a[p] = temp

	i = 1
	j = n - 1
	while True:
		while i < n and a[i] <= a[0]:
			i += 1
		while j >= 1 and a[j] > a[0]:
			j -= 1
		if j < i:
			break
		else:
			temp = a[i]
			a[i] = a[j]
			a[j] = temp

	temp = a[0]
	a[0] = a[j]
	a[j] = temp

	return a, j

# quick_select
def quick_select(a, k):
	p = choose_pivot(a)

	a, i = partition(a, p)
	
	# if we have found the right index, return it's value
	if i == k:
		return a[i]

	# if the index is larger than the k we want, recurse on first elements up to i
	# still looking for index k of these elements
	elif i > k:
		return quick_select(a[:i], k)

	# if index is smaller than k we want, we recurse on the latter have of the elements
	# our new index of interest is k - i - 1, since we ignore the first elements up to i
	else:
		return quick_select(a[i + 1:], k - i - 1)

def quick_sort(a):
	if len(a) <= 1: 
		return a
	p = choose_pivot(a)
	a, i = partition(a, p)
	left = quick_sort(a[:i])
	right = quick_sort(a[i + 1:])
	return left + [a[i]] + right



