# A heap is a type of binary tree in which each node has a greater (min-heap) value than its parent. In a max-heap this is reversed.
# It is a balanced tree such that all levels are fill except for the last one, and the last level is left-justified (i.e never fill in a right child befre the left one)
# Each node in the tree has access to its parent

# Operations
    # insert: involves placing the new node at the next position and applying bubble_up to reorder 
    # delete_max/min: in a max-heap, delete_max will remove the root and bubble_down to resolve

# Here we implement a MAX HEAP as an array
# For an array A and some index i
# left_child = A[2 * i + 1]
# right_child = A[2 * i + 2]
# parent = A[floor((i - 1) / 2))], unless i == 0

import math

class heap:
    def __init__(self, a):
        # assume intial array a is not a heap, so we insert each to create a proper heap
        self.a = a
        self.heapify()

    # O(n) algorithm for converting an array into a heap
    def heapify(self):
        n = len(self.a)
        for i in reversed(xrange(int(math.floor(n/2.)) + 1)):
            self.bubble_down(i)

    def insert(self, value):
        self.a.append(value)
        self.bubble_up(len(self.a) - 1)
    
    def bubble_up(self, i):
        current_index = i
        while self.parent(current_index) != None and self.parent(current_index) < self.a[current_index]:
            temp = self.a[current_index]
            self.a[current_index] = self.parent(current_index)
            self.a[self.parent_index(current_index)] = temp
            current_index = self.parent_index(current_index)
    
    def delete_max(self):
        max_value = self.a[0]
        self.a[0] = self.a[-1]
        drop_value = self.a.pop()
        self.bubble_down(0)
        return max_value

    def bubble_down(self, i):
        current_index = i
        while not self.is_leaf(current_index):
            max_child = (2 * current_index + 1) if self.left_child(current_index) > self.right_child(current_index) else (2 * current_index + 2)
            if self.a[max_child] > self.a[current_index]:
                temp = self.a[current_index]
                self.a[current_index] = self.a[max_child]
                self.a[max_child] = temp
                current_index = max_child
            else:
                break

    def is_leaf(self, i):
        return self.left_child(i) == None

    def left_child(self, i):
        if 2 * i + 1 >= len(self.a):
            return None
        return self.a[2 * i + 1]
    
    def right_child(self, i):
        if 2 * i + 2 >= len(self.a):
            return None
        return self.a[2 * i + 2]
    
    def parent_index(self, i):
        return int(math.floor((i - 1) / 2.))
        
    def parent(self, i):
        if i == 0:
            return None
        return self.a[self.parent_index(i)]
