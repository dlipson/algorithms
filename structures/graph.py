# A graph is made up of vertices and edges between those vertices
# Here we implement a directed graph


class graph:
	# for ease, we will assume v are enumerate 0...n-1
	def __init__(self, n, e):
		self.n = n
		self.matrix = [[0]*n for i in xrange(n)]
		for (a, b) in e:
			self.matrix[a][b] = 1

	# determine if there is a path from a to b
	# uses a QUEUE, i.e layer by layer
	def breadth_first_search(self, a, b):
		queue = []
		visited = [False for i in xrange(self.n)]
		queue.append(a)
		visited[a] = True

		while len(queue) > 0:
			current = queue.pop(0)
			neighbours = [i for i in xrange(self.n) if self.matrix[current][i] == 1]
			for n in neighbours:
				if n == b:
						return True
				if not visited[n]:
					queue.append(n)
					visited[n] = True

		return False

	# uses a STACK, i.e full path before returning
	def depth_first_search_iterative(self, a, b):
		stack = []
		visited = [False for i in xrange(self.n)]
		visited[a] = True
		stack.insert(0, a)

		while len(stack) > 0:
			current = stack.pop(0)
			neighbours = [i for i in xrange(self.n) if self.matrix[current][i] == 1]
			for n in neighbours:
				if n == b:
					return True
				if not visited[n]:
					stack.insert(0, n)
					visited[n] = True

		return False

	def depth_first_search_recursive(self, a, b):
		visited = [False for i in xrange(self.n)]
		visited[a] = True
		return self.depth_first_search_recursive_helper(a, b, visited) == True

	def depth_first_search_recursive_helper(self, a, b, visited):
		if a == b:
			return True
		neighbours = [i for i in xrange(self.n) if self.matrix[a][i] == 1]
		for n in neighbours:
			if not visited[n]:
				visited[n] = True
				visited = self.depth_first_search_recursive_helper(n, b, visited)
				if visited == True:
					return True
		return visited




