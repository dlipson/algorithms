# A linked list is a structure of forward-linked nodes that store some value or data and may be sorted or unsorted.

# Operations:
#   insert: If sorted, O(n), else insert to front, O(1)
#   delete_max: If sorted, O(1), else must find max value, O(n)
#   search: Not given here, but simply search for a node as seen in the format of delete, where instead we search for max_value, not deleting it.

# linked list node
class node:
    def __init__(self, value, next):
        self.value = value
        self.next = next

class linked_list:
    def __init__(self):
        self.root = None

    def output(self):
        current_node = self.root
        while current_node != None:
            print(current_node.value)
            current_node = current_node.next

class unsorted_linked_list(linked_list):
    def insert(self, v):
        current_root = self.root
        self.root = node(v, current_root) 

    def delete_max(self):
        root = self.root

        if root == None:
            return

        # search for the node with the max value
        max_value = root.value
        current_node = root
        while current_node != None:
            if current_node.value > max_value:
                max_value = current_node.value
            current_node = current_node.next

        # delete the node with max_value
        if root.value == max_value:
            self.root = root.next
            return

        previous_node = root
        while previous_node != None:
            current_node = previous_node.next
            if current_node.value == max_value:
                previous_node.next = current_node.next
                break
            previous_node = previous_node.next

class sorted_linked_list(linked_list):
    def insert(self, v):
        current_root = self.root
        if self.root == None or self.root.value <= v:
            self.root = node(v, current_root)
        else:
            current_node = self.root
            while current_node.next != None and current_node.next.value > v:
                current_node = current_node.next
            if current_node.next == None:
                current_node.next = node(v, None)
            else:
                temp_node = current_node.next
                current_node.next = node(v, temp_node)

    def delete_max(self):
        if self.root != None:
            current_root = self.root
            self.root = current_root.next
