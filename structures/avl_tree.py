# An AVL Tree is a BST with the additional property that no two children different by a height of more than +/- 1
# Each non-empty node stores a value of [-1,0,1] = height(right) - height(left)
# This makes the tree a more efficient tree to operate on, on average

# Operations: Same as BST, applied differently

