# A trie is a tree in which each branch makes up a sequence or string, and each level indicates the character at that index in the sequence
# A sequence might have subsequences which are either flagged if they are true sequences, or unflagged if they are pathways

# Operations
    # insert: Add a string/sequence path to a trie if it is not already there; flag if subsequence exists; proportional to the height of the trie
    # delete: Remove a flag if a subsequence; remove all ancestors if only-child recursively, until reaching a flag or a fork

class node:
    def __init__(self, char):
        self.char = char
        self.children = []
        self.is_end = False

class trie:
    def __init__(self):
        self.root = node("")

    def insert(self, string):
        current = self.root
        for c in string:
            next = None
            for n in current.children:
                if n.char == c:
                    next = n
                    break
            if next == None:
                next = node(c)
                current.children.append(next)
            current = next
        current.is_end = True

    def output(self, node):
        for i in node.children:
            print i.char,
        print ""
        for i in node.children:
            self.output(i)

    def search(self, string):
        current = self.root
        for c in string:
            next = None
            for n in current.children:
                if n.char == c:
                    next = n
                    break
            if next == None:
                return False
            current = next
        return current.is_end
        
    def delete(self, string):
        current = self.root
        nodes = [current]
        for c in string:
            next = None
            for n in current.children:
                if n.char == c:
                    next = n
                    break
            if next == None:
                return
            nodes.append(next)
            current = next
        nodes = list(reversed(nodes))
        if not current.is_end:
            return
        if len(current.children) != 0:
            current.is_end = False
        else:
            for i in xrange(len(nodes) - 1):
                parent = nodes[i + 1]
                current = nodes[i]
                parent.children.remove(current)
                if parent.is_end or len(parent.children) != 0:
                    return
            self.root.is_end = False