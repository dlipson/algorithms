# A binary search tree is made of nodes that store some value and have 2 children (left and right) such that the value in the left node is smaller than the parent, and the right node is larger than the parent.

# Operations:
#   insert: Requires searching the height of a branch on the tree (at worst, the height is n)
#   delete: Different cases depending on number of children, but again proportional to height
#   search

# linked list node
class node:
    def __init__(self, value, left, right):
        self.value = value
        self.left = left
        self.right = right

# pre-order output
def pre_output(root):
    if root != None:
        print(root.value)
        pre_output(root.left)
        pre_output(root.right)

# in-order output
def in_output(root):
    if root != None:
        pre_output(root.left)
        print(root.value)
        pre_output(root.right)

# post-order output
def post_output(root):
    if root != None:
        pre_output(root.left)
        pre_output(root.right)
        print(root.value)


# insert
def insert(root, v):
    if root == None:
        root = node(v, None, None)
    elif root.value > v:
        root.left = insert(root.left, v) 
    elif root.value < v:
        root.right = insert(root.right, v)
    return root

# delete
def delete(root, v):
    if root == None:
        return None
    if root.value == v:
        # case 1: no children
        if root.left == None and root.right == None:
            return None
        # case 2: one child
        elif root.right == None:
            return root.left
        elif root.left == None:
            return root.right
        # case 3: two children
        else:
            min_node = root.right
            while min_node.left != None:
                min_node = min_node.left
            root.value = min_node.value
            min_node.value = v
            min_node = delete(min_node, v)
    elif root.value > v:
        root.left = delete(root.left, v)
    else:
        root.right = delete(root.right, v)
    return root



